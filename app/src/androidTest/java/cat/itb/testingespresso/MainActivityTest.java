package cat.itb.testingespresso;

import android.app.Activity;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withHint;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    public static String USER_TO_BE_TYPED = "Victor";
    public static String PASS_TO_BE_TYPED = "Contrasenya";

    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule
            = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void elements_diplayed_correctly() {
        onView(withId(R.id.textView)).check(matches(isDisplayed()));
        onView(withId(R.id.button)).check(matches(isDisplayed()));
    }

    @Test
    public void check_items_text_is_correct(){
        onView(withId(R.id.textView)).check(matches(withText("Main Activity title")));
        onView(withId(R.id.button)).check(matches(withText("Next")));
    }

    @Test
    public void check_button_is_clickable(){
        onView(withId(R.id.button)).check(matches(isClickable()));
        onView(withId(R.id.button)).perform(click()).check(matches(withText("Back")));
    }

    @Test
    public void login_form_behaviour(){
        onView(withId(R.id.username)).perform(typeText(USER_TO_BE_TYPED), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(PASS_TO_BE_TYPED), closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click()).check(matches(withText("Logged")));
    }


    @Test
    public void changeToActivity2(){
        onView(withId(R.id.button)).perform(click());
        matches(withId(R.id.activity2));

    }

    @Test
    public void mainActivityToActivity2AndActivity2ToMainActivity() {
        onView(withId(R.id.button)).perform(click());
        matches(withId(R.id.activity2));
        onView(withId(R.id.buttonBack)).perform(click());
        matches(withId(R.id.mainActivity));
    }

    @Test
    public void largeTestFunction(){
        onView(withId(R.id.username)).perform(typeText(USER_TO_BE_TYPED), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(PASS_TO_BE_TYPED), closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click());
        matches(withId(R.id.activity2));
        onView(withId(R.id.textView2)).check(matches(withText("Welcome back " + USER_TO_BE_TYPED)));
        onView(withId(R.id.buttonBack)).perform(click());
        matches(withId(R.id.mainActivity));
        onView(withId(R.id.username)).check(matches(withText("")));
        onView(withId(R.id.password)).check(matches(withText("")));
    }



}
